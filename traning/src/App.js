import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  constructor(props) {
     super(props)
     this.state = {
        students: [
           { id: 1, name: 'Wasif', age: 21, email: 'wasif@email.com' },
           { id: 2, name: 'Ali', age: 19, email: 'ali@email.com' },
           { id: 3, name: 'Saad', age: 16, email: 'saad@email.com' },
           { id: 4, name: 'gggh', age: 16, email: 'gggh@email.com' },
           { id: 5, name: 'Hanam', age: 25, email: 'Hanam@email.com' },
           { id: 6, name: 'Debi', age: 23, email: 'Debi@email.com' },
           { id: 7, name: 'And', age: 26, email: 'and@email.com' },
           { id: 8, name: 'Ror', age: 29, email: 'ror@email.com' },
           { id: 9, name: 'James', age: 20, email: 'jamesd@email.com' }
        ]
     }
     this.sortByageAsc = this.sortByageAsc.bind(this);
    this.sortByageDesc = this.sortByageDesc.bind(this);
  }



//   renderTableHeader() {
//    let header = Object.keys(this.state.students[0])
//    return header.map((key, index) => {
//       return <th key={index}>{key.toUpperCase()}</th>
//    })
// }

// sorting(){
   
// }
// renderTableData() {
//    return this.state.students.map((student, index) => {
//       const { id, name, age, email } = student //destructuring
//       return (
//          <tr key={id}>
//             <td>{id}</td>
//             <td>{name}</td>
//             <td>{age}</td>
//             <td>{email}</td>
//          </tr>
//       )
//    })
// }

// sortByageAsc() {
//    this.setState(prevState => {
//      this.state.students.sort((a, b) => (a.age - b.age))
//  });
//  }

//  sortByageDesc() {
//    this.setState(prevState => {
//      this.state.students.sort((a, b) => (b.age - a.age))
//  });
//  }
 sortByageAsc=()=>{

   let sorteddetailAsc;
   sorteddetailAsc= this.state.students.sort((a,b)=>{
      return parseInt(a.age)  - parseInt(b.age);
   })

   this.setState({
       students:sorteddetailAsc
   })
}


sortByageDesc=()=>{

   let sorteddetailDsc;
   sorteddetailDsc= this.state.students.sort((a,b)=>{
      return parseInt(b.age)  - parseInt(a.age);
   })

   this.setState({
      students:sorteddetailDsc
   })
}
render() {
   return (
     
      <div>
      Sorting:
        <button onClick={this.sortByageAsc}>
        Ascending
        </button>
        <button onClick={this.sortByageDesc}>
        descending
        </button>
      <div className="student">
      { this.state.students.map((student, index) =>
        <div key ={index} className="card">
          <h3>Id { student.id } </h3>
          <p>Name: { student.name }</p>
          <p>age: { student.age}</p>
          <p>email: { student.email}</p>
        </div>
      )}
      </div>
      </div>
   )
}
}

ReactDOM.render(<App />, document.getElementById('root'));
 export default App;
