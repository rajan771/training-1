import { Platform, StyleSheet, View, Text } from 'react-native';
 
export default class Acsdcs extends Component()
{
 
    render()
    { 
        var Array_1 = [10, 5, 25, 15, 7, 50, 20, 100];

        // Sorting array in Ascending order.
        Array_1.sort(function(a, b){return a-b});


        var Array_2 = [10, 5, 25, 15, 7, 50, 20, 100];

        // Sorting array in Descending order.
        Array_2.sort(function(a, b){return b-a});

        return(
 
            <View style = { styles.MainContainer }>

                    <Text style={styles.text}> {Array_1.toString()}</Text>

                    <Text style={styles.text}> {Array_2.toString()}</Text>
            
            </View>
            
        );
    }
}
 
const styles = StyleSheet.create(
{
    MainContainer:
    {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
    },

    text:{

        fontSize: 20,
        textAlign: 'center',
        margin:10
    }
});