import React from "react";
import ReactDOM from 'react-dom';
const ObjectTest = {
   1: {
      id : 1,
      name:'kumar',
      mark:70
  },
  2: {
      id: 2,
      name:'ram',
      mark:75
  },
  3: {
    id: 3,
    name:'raj',
    mark:85
},
4:{
  id: 4,
  name:'sanjay',
  mark:35
},
5:{
  id: 5,
  name:'karupu',
  mark:45
},
6:{
  id: 6,
  name:'sankar',
  mark:55
},
7:{
  id: 7,
  name:'pavi',
  mark:50
},
8:{
  id: 8,
  name:'Tamil',
  mark:90
},
9:{
  id: 10,
  name:'sanjay',
  mark:95
},
10:{
  id: 10,
  name:'mani',
  mark:55
}
}

class App extends React.Component{

_renderObject(){
  return Object.entries(ObjectTest).map(([key, value], i) => {
    return (
      <div key={key}>
        id is: {value.id} ;
        name is: {value.name};
        mark is:{value.mark}
      </div>
    )
  })
}

render(){
  return(
    <div>
      {this._renderObject()}
    </div>
  )
}
}

ReactDOM.render(<App/>, document.getElementById('app'))
